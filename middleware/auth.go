package middleware

import (
	"context"
	"strconv"

	"gitlab.com/microservice-workshop/utils-go.git/model"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc/metadata"
)

var userCtxKey = &contextKey{"user"}

type contextKey struct {
	name string
}

func Authorize() gin.HandlerFunc {
	return func(c *gin.Context) {

		userID := c.GetHeader("user-id")
		username := c.GetHeader("username")

		if userID == "" || userID == "undefined" {
			c.Next()
			return
		}

		intUserID, err := strconv.Atoi(userID)
		if err != nil {
			panic(err)
		}

		ctx := context.WithValue(c.Request.Context(), userCtxKey, &model.User{
			ID:       intUserID,
			Username: username,
		})

		md := metadata.New(map[string]string{
			"id":       userID,
			"username": username,
		})

		ctx = metadata.NewOutgoingContext(ctx, md)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}

func AuthContext(ctx context.Context) *model.User {
	raw, _ := ctx.Value(userCtxKey).(*model.User)
	return raw
}
